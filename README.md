# README #


## What is this repository for? ###

This repository provides jupyter notebooks to generate 11 figures of a manuscript accepted for publication in 
Current Biology in July 2018

1.The figure notebooks are in the folder called /FiguresNoteBooks

2.There is also a folder (/HTMLViewOfFiguresNOteBooks) containing the html version of the notebooks for easy visualisation of the codes and figures

3.The folder /load_preprocess_mouse contains notebooks for preprocessing of neuronal and behavioral data 

4.A text file with a list of libraires packages used (package_librariesRequierments.txt)

##  1) To reproduced the figures, you need to download this entire repository that contains

##### A. The notebooks that contain the codes to analyse and generate the figures
##### B. The requirement text file contains the versions of librarires and packages necessary to run the notebooks. 
If you used the specific version of these librairies, in theory you should be able to reproduced the notebook forever

Step-by-step instructions are given below

##### C. you need the neurophysiological and behavioral data used by these notebooks. 
These are not the raw electrophysiological data (too big). Just the processed electrophysiological and behavioral data
You can download them here : http://dx.doi.org/10.17632/4hv73sgb5b.1

### 2) you will need install the jupyter notebook.
The simplest way is to install it through anaconda (http://jupyter.org/install). 

### 3) install the required libraries in a specific environment
______________________________________________________________________________________________
## Step by Step instructions 

The instructions workd under ubuntu 18.04 but should be similar for other systems. 
you will need to have already jupyer installed  and the data downloaded (see 2) and 3) above)

1) first install git (if you don't have it already)

In a terminal type

sudo apt-get install git-core

then navigate into a folder where you will download the repository. for instance type :

cd /home/robbe/Documents/

2) clone this repository. 
click on the clone button (in this web page) and the copy icon.
go back to your terminal and past. this will create a folder called davidrobbe-sales-cabonell_manuscript with notebooks inside

3) navigate in that folder where you dowloaded this repository

cd /home/robbe/Documents/davidrobbe-sales-cabonell_manuscript/

4) create an environment (I named it SalesMS) where you will install the specifci packages/libraries

type in the same terminal:

python3 -m virtualenv SalesMS

source SalesMS/bin/activate

pip install -r package_librariesRequierments.txt

pip install ipykernel

pip install -I path.py==7.7.1

ipython kernel install --user --name=SalesMS

5) run the jypyer notebook
in a terminal type :
jupyter notebook

This last comment will start the jupyter notebook. you need to need to navigate again to the folder that contain the figure notebooks

When you open a jupyter notebook you need to go to kernel and change the kernel to use SalesMS instead of python 3.



#### 


When you run the notebooks you will have to change the path for the data and preprocessing notebook (in the first cell) 




## Who do I talk to? ###

* david.robbe@inserm.fr